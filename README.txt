This module is deprecated! Please use nodewords 6.x-1.2 or higher instead

General information about canonical URLs can be found at http://www.valthebald.net/canonical-url-drupal.html

Module settings page can be accessed via admin/Content management/Canonical URL
(admin/content/canonical_url).

Starting from version 6.x-1.3, module declares own user access permissions:
'administer canonical url' (access to canonical url settings page, admin/content/canonical_url),
and 'override node canonical url' (access to canonical url tab in node editing form)

Module operates in 2 modes: automatic and manual ('Automatic mode' checkbox in settings page).
In manual mode, canonical url is switched off by default, and is shown only for nodes with canonical url
explicitly set in node editing form ('Canonical URL' tab).
In automatic mode, canonical url is shown for all nodes. Default value is node path alias, which can be
overriden in node editing form, 'Canonical URL' tab.

Your questions and comments are welcome at http://www.valthebald.net/contact.html

Changelog:

version 6.x-1.3:
* Own permission set ('administer canonical url' and 'override node canonical url')
* Automatic mode
* Small bit of documentation :) 