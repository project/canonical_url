<?php
/**
 * @file
 * Assign META tags to nodes, vocabularies, terms and pages.
 */

/**
 * Menu callback: settings form.
 */
function canonical_url_admin_settings_form() {
  $form['global'] = array(
    '#type' => 'fieldset',
    '#title' => t('Global settings'),
  );
  
  $form['global']['canonical_url_automode'] = array(
    '#type' => 'checkbox',
    '#title' => t('Automatic mode'),
    '#default_value' => variable_get('canonical_url_automode', '0'),
    '#description' => t('In automatic mode, canonic URL is added to all nodes, using path alias as default'),
  );

  $form['global']['canonical_url_append_domain'] = array(
    '#type' => 'checkbox',
    '#title' => t('Append domain to URLs'),
    '#default_value' => variable_get('canonical_url_append_domain', '1'),
    '#description' => t('Mark to add domain to canonical URL'),
  );
  
  $form['global']['canonical_url_domain'] = array(
    '#type' => 'textfield',
    '#title' => t('Domain'),
    '#default_value' => variable_get('canonical_url_domain', $_SERVER['HTTP_HOST']),
    '#size' => 60,
    '#maxlength' => 255,
  );
  
  $form['global']['canonical_url_proto'] = array(
    '#type' => 'select',
    '#title' => t('Protocol'),
    '#options' => drupal_map_assoc(array('http', 'https')),
    '#default_value' => variable_get('canonical_url_proto', 'http'),
  );
  
  return system_settings_form($form);  
}

/**
 * Menu callback: front page settings form.
 */
function canonical_url_admin_frontpage_form(&$form_state) {
  $tags = _canonical_url_load('page', '');

  $form = array();
  $form['canonical_url'] = _canonical_url_form('page', $tags);

  unset($form['canonical_url']['#title']);
  unset($form['canonical_url']['#type']);
  if (empty($form['canonical_url'])) {
    $form['canonical_url'] = array('#value' => t('Currently no meta tags can be assigned to the front page because you have disabled all tags to show on the edit forms. <a href="!canonical_url-settings-url" title="meta tags settings">Enable some meta tags to show on edit forms</a> first.', array('!canonical_url-settings-url' => url('admin/content/canonical_url'))));
  }
  elseif (!variable_get('canonical_url-use_front', 1)) {
    $form['canonical_url'] = array('#value' => t('You can not assign meta tags for the front page here because you have disabled them at the <a href="!canonical_url-settings-url" title="Meta tags settings">meta tags settings page</a>. Instead, the meta tags for the view, panel or node you have set as front page will be used.', array('!canonical_url-settings-url' => url('admin/content/canonical_url'))));
  }
  else {
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Save'),
      '#weight' => 40,
    );
  }

  return $form;
}

function canonical_url_admin_frontpage_form_submit($form, &$form_state) {
  if ($form_state['values']['op'] == t('Save')) {
    _canonical_url_set('page', '', $form_state['values']['canonical_url']);
    drupal_set_message(t('The meta tags for the front page have been saved.'));
    $form_state['redirect'] = 'admin/content/canonical_url/frontpage';
  }
}
